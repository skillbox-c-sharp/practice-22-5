﻿using System;

namespace Challenge1
{
    internal class Program
    {
        private const string digits = "0123456789ABCDEF";

        static void Main(string[] args)
        {
            while (true)
            {
                string numberString = Console.ReadLine();
                Console.WriteLine($"Число: {ParseNumber(numberString)}");
            }
        }

        private static double ParseNumber(string numberString)
        {
            if (numberString == null || numberString.Length == 0)
                return double.NaN;

            numberString = numberString.ToUpper();

            int sign = 1;
            int baseSystem = 0;
            double val = 0;
            int i = 0;

            if (SetNegative(numberString[0], out sign))
            {
                sign = -1;
                i++;
            }

            if (i > 0 && i == numberString.Length)
                return double.NaN;

            baseSystem = SetBaseSystem(numberString, ref i);

            if (i == numberString.Length)
                return double.NaN;

            if(!SetValue(ref i, ref val, numberString, baseSystem)) 
                return double.NaN;

            RemoveZeros(ref numberString, i);

            if(!SetResult(numberString, ref i, baseSystem, ref val))
                return double.NaN;

            double result = sign * val;

            if (result > 1.7976931348623157E+308 || result < -1.7976931348623157E+308) return double.NaN;

            return result;
        }

        private static bool SetResult(string numberString, ref int i, int baseSystem, ref double val)
        {
            double power = 1;
            while (i < numberString.Length)
            {
                char c = numberString[i];
                int d = digits.IndexOf(c);
                if (d == -1 || d >= baseSystem)
                    return false;
                power *= baseSystem;
                val = baseSystem * val + d;
                i++;
            }
            val = val / power;
            return true;
        }

        private static bool SetValue(ref int i, ref double val, string numberString, int baseSystem)
        {
            while (i < numberString.Length)
            {
                char c = numberString[i];
                if ((c == '.' || c == ','))
                {
                    i++;
                    break;
                }
                int d = digits.IndexOf(c);
                if (d == -1 || d >= baseSystem)
                    return false;
                val = baseSystem * val + d;
                i++;
            }
            return true;
        }

        private static void RemoveZeros(ref string numberString, int i)
        {
            int j = numberString.Length - 1;
            while (j != i)
            {
                if (numberString[j] == '0') j--;
                else break;
            }
            numberString = numberString.Substring(0, j + 1);
        }

        private static bool SetNegative(char signChar, out int sign)
        {
            if (signChar == '-')
            {
                sign = -1;
                return true;
            }
            sign = 1;
            return false;
        }

        private static int SetBaseSystem(string numberString, ref int i)
        {
            //0x - шестнадцатиричная
            if (numberString[i] == '0' && (i + 1 != numberString.Length) && numberString[i + 1] == 'X')
            {
                i += 2;
                return 16;
            }
            //0b - двоичная
            else if (numberString[i] == '0' && (i + 1 != numberString.Length) && numberString[i + 1] == 'B')
            {
                i += 2;
                return 2;
            }
            //0c - восьмиричная
            else if (numberString[i] == '0' && (i + 1 != numberString.Length) && numberString[i + 1] == 'C')
            {
                i += 2;
                return 8;
            }
            return 10;
        }
    }
}